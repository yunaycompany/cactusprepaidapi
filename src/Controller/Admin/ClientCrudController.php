<?php

namespace App\Controller\Admin;

use App\Entity\Client;
use EasyCorp\Bundle\EasyAdminBundle\Config\Crud;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\DateTimeField;
use EasyCorp\Bundle\EasyAdminBundle\Field\FormField;
use EasyCorp\Bundle\EasyAdminBundle\Field\IdField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;

class ClientCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return Client::class;
    }


    public function configureFields(string $pageName): iterable
    {
        $fields= [
            IdField::new('id')->onlyOnIndex(),
            TextField::new('documentNumber'),
            TextField::new('cardType')->formatValue(function ($value) {
                return $value =='plastic-eur'? 'EUR': "USD";
            }),
            TextField::new('gender'),
            DateTimeField::new('birthDate')
        ];


        if (Crud::PAGE_INDEX === $pageName) {
            return $fields;
        } elseif(Crud::PAGE_EDIT=== $pageName) {
          $fieldsDetail=[
              FormField::addPanel('Address'),
              TextField::new('address.town','Town'),
              TextField::new('address.bornCity', 'Born City'),
              TextField::new('address.bornCountry', 'Born Country'),
              TextField::new('address.address', 'Address'),
              TextField::new('address.phoneCode', 'Phone Code'),
              TextField::new('address.phoneNumber', 'Phone Number'),
              TextField::new('address.postalCode', 'Postal Code'),
              TextField::new('address.country', 'Country'),
              TextField::new('address.state', 'State'),
          ];
          return array_merge($fields, $fieldsDetail);
        }
    }

}
