<?php

namespace App\Controller\Admin;

use App\Entity\PrepaidCard;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\BooleanField;
use EasyCorp\Bundle\EasyAdminBundle\Field\DateTimeField;
use EasyCorp\Bundle\EasyAdminBundle\Field\IdField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;

class PrepaidCardCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return PrepaidCard::class;
    }


    public function configureFields(string $pageName): iterable
    {
        return [
            IdField::new('id')->onlyOnIndex(),
            TextField::new('cardNumber'),
            TextField::new('cardHolderName'),
            TextField::new('brand'),
            TextField::new('client'),
            BooleanField::new('status'),
            DateTimeField::new('created'),
            DateTimeField::new('updated'),
        ];
    }

}
