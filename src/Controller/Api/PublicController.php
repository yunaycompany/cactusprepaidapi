<?php

namespace App\Controller\Api;

use App\Entity\Country;
use App\Entity\State;
use App\Services\RestMailer;
use App\Services\UserService;
use Symfony\Component\HttpFoundation\Request;
use App\Entity\User;
use Symfony\Component\Routing\Annotation\Route;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class PublicController extends AbstractController
{
    protected $userService;

    protected $restMailer;


    public function __construct(UserService $userService, RestMailer $restMailer)
    {
        $this->userService = $userService;

        $this->restMailer = $restMailer;
    }


    /**
     * @Route("/login", methods={"POST"})
     *
     */
    public function login(Request $request)
    {
        $params = $request->request->all();
        $response = $this->userService->loginUser($params);

        return $this->json($response);

    }


    /**
     * @Route("/card", methods={"POST"}, name="post_card")
     */
    public function postCard(Request $request)
    {
        $params = $request->request->all();
        $response = $this->userService->createCardRequest($params);
        return $this->json($response);
    }


    /**
     * @Route("/signup", methods={"POST"})
     *
     */
    public function register(Request $request)
    {
        $params = $request->request->all();
        $response = $this->userService->registerUser($params);

        return $this->json($response);
    }
    /**
     * @Route("/contact", methods={"POST"})
     */
    public function contact(Request $request)
    {
        $params = $request->request->all();
        $result = $this->userService->contact($params);

        return $this->json($result);
    }


    /**
     * @Route("/confirm/{token}", methods={"POST"})
     */
    public function postConfirm($token)
    {

        $result = $this->userService->confirm($token);

        return $this->json($result);
    }

    /**
     * @Route("/confirmOne", methods={"POST"})
     */
    public function postConfirmOne(Request $request)
    {
        $params = $request->request->all();
        $regUser = $this->userService->confirmOne($params);
        if ($regUser instanceof User) {
            $result = array('status' => 'OK');
        } else {
            $result = array('status' => 'error', 'response' => $regUser);
        }
        return $this->json($result);
    }


    /**
     * @Route("/confirmTwo", methods={"POST"})
     *
     */
    public function postConfirmTwo(Request $request)
    {
        $params = $request->request->all();
        $regUser = $this->userService->confirmTwo($params);

        if ($regUser instanceof User) {
            $result = array('status' => 'OK');
        } else {
            $result = array('status' => 'error', 'response' => $regUser);
        }
        return $this->json($result);
    }


    /**
     * @Route("/confirmThree", methods={"POST"})
     *
     */
    public function postConfirmThree(Request $request)
    {
        $token = $request->get('confirmation_token');
        $files = array(
            'id' => $request->files->get('idFile'),
            'servicio' => $request->files->get('serviceFile')
        );
        $regUser = $this->userService->confirmThree($token, $files);
        if ($regUser instanceof User) {
            $result = array('status' => 'OK');
        } else {
            $result = array('status' => 'error', 'response' => $regUser);
        }
        return $this->json($result);
    }


    /**
     * @Route("/country", methods={"GET"})
     *
     */
    public function getCountry()
    {
        $response['country'] = $this->getDoctrine()->getRepository(Country::class)->allArray();;
        return $this->json($response);
    }

    /**
     * @Route("/state/{countryId}", methods={"GET"})
     *
     */
    public function getState($countryId)
    {

        $response['state'] = $this->getDoctrine()->getRepository(State::class)->allArray($countryId);;
        return $this->json($response);
    }


    /**
     * @Route("/forgotPass", methods={"POST"})
     */
    public function postForgotPass(Request $request)
    {
        $params = $request->request->all();
        $response = $this->userService->recoveryPassword($params);
        return $this->json($response);
    }

    /**
     * @Route("/resetPass", methods={"POST"})
     */
    public function postResetPassAction(Request $request)
    {
        $params = $request->request->all();
        $regUser = $this->userService->resetPassword($params);
        $result = array('status' => 'error', 'response' => $regUser);
        if ($regUser instanceof User) {
            $result = array('status' => 'OK');
        }
        return $this->json($result);
    }


}
