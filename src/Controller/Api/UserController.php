<?php

/**
 * Created by PhpStorm.
 * User: yosbel
 * Date: 06/04/17
 * Time: 02:12
 */

namespace App\Controller\Api;


use App\Entity\PrepaidCard;
use App\Entity\User;

use App\Enum\RolType;
use App\Services\UserService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;

class UserController extends AbstractController
{
    protected $userService;

    /**
     * UserController constructor.
     */
    public function __construct(UserService $userService)
    {
        $this->userService = $userService;
    }


    /**
     *Get User
     * @Route("/user",methods={"GET"})
     */
    public function getMyUser()
    {
        $user = $this->getUser();


        $response["info"] = ["firstName" => $user->getFirstName(),
            "lastName" => $user->getLastNameFather().' '.$user->getLastNameMother(),

            "email" => $user->getEmail()];


        return $this->json($response);

    }

    /**
     *Get User
     * @Route("/user/cards",methods={"GET"})
     */
    public function getUserCards()
    {
        $user = $this->getUser();
        $roleA =$user->hasRole(RolType::ROLE_ADMIN);
        $cards = $this->getDoctrine()->getRepository(PrepaidCard::class)->findUserCards($user,$roleA);

        $response["cards"] = $cards;


        return $this->json($response);

    }

    /**
     *Get Cards Request
     * @Route("/user/request",methods={"GET"})
     */
    public function getCardsRequest()
    {
        $cards =$this->userService->getCardsRequest();

        $response["request"] = $cards;


        return $this->json($response);

    }


    /**
     * @Route("/user/changePass", methods={"POST"})
     *
     */
    public function postChangePassAction(Request $request)
    {
        $regUser = $this->userService->changePassword($request->request->all());
        if ($regUser) {
            $result = array('status' => 'OK');
        } else {
            $result = array('status' => 'error', 'response' => $regUser);
        }
        return $this->json($result);
    }


}
