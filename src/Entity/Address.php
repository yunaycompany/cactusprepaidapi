<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use JMS\Serializer\Annotation as JMS;
/**
 *
 *
 * @ORM\Table(name="address")
 * @ORM\Entity
 */
class Address
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     * @JMS\Expose
     *
     */
    protected $id;

    /**
     * @var string
     *
     * @ORM\Column(name="city", type="string", nullable=true)
     * @JMS\Expose
     * @JMS\Groups("cards")
     * @JMS\SerializedName("city")
     */
    protected $town;


    /**
     * @var string
     *
     * @ORM\Column(name="born_city", type="string", nullable=true)
     * @JMS\Expose
     * @JMS\Groups("cards")
     *
     */
    protected $bornCity;


    /**
     *
     * @ORM\ManyToOne(targetEntity="Country")
     * @ORM\JoinColumn(name="born_country_id", referencedColumnName="id")
     *
     */
    protected $bornCountry;

    /**
     * @var string
     *
     * @ORM\Column(name="address", type="string", nullable=true)
     * @JMS\Expose
     * @JMS\Groups("cards")
     */
    protected $address;


    /**
     * @var string
     *
     * @ORM\Column(name="phone_code", type="string", nullable=true)
     *
     */
    protected $phoneCode;


    /**
     * @var string
     *
     * @ORM\Column(name="phone_number", type="string", nullable=true)
     *
     */
    protected $phoneNumber;


    /**
     * @var string
     *
     * @ORM\Column(name="postal_code", type="string", nullable=true)
     * @JMS\Expose
     * @JMS\Groups("cards")
     *  @JMS\SerializedName("postalCode")
     */
    protected $postalCode;

    /**
     *
     * @ORM\ManyToOne(targetEntity="Country")
     * @ORM\JoinColumn(name="country_id", referencedColumnName="id")
     *
     *
     */
    protected $country;

    /**
     *
     * @ORM\ManyToOne(targetEntity="State")
     * @ORM\JoinColumn(name="state_id", referencedColumnName="id")
     *
     */
    protected $state;

    /**
     * Constructor
     */
    public function __construct($params = [])
    {
        $this->setValues($params);
    }

    public function setValues($params)
    {
        $this->town = isset($params['city']) ? $params['city'] : $this->town;
        $this->bornCity = isset($params['bornCity']) ? $params['bornCity'] : $this->bornCity;
        $this->bornCountry = isset($params['bornCountry']) ? $params['bornCountry'] : $this->bornCountry;
        $this->address = isset($params['address']) ? $params['address'] : $this->address;
        $this->phoneCode = isset($params['phoneCode']) ? $params['phoneCode'] : $this->phoneCode;
        $this->phoneNumber = isset($params['phoneNumber']) ? $params['phoneNumber'] : $this->phoneNumber;
        $this->postalCode = isset($params['zip']) ? $params['zip'] : $this->postalCode;
        $this->country = isset($params['country']) ? $params['country'] : $this->country;
        $this->state = isset($params['state']) ? $params['state'] : $this->state;

        return $this;
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getAddress()
    {
        return $this->address;
    }

    /**
     * @param string $address
     */
    public function setAddress( $address): void
    {
        $this->address = $address;
    }

    /**
     * @return string
     */
    public function getBornCity()
    {
        return $this->bornCity;
    }

    /**
     * @param string $bornCity
     */
    public function setBornCity($bornCity): void
    {
        $this->bornCity = $bornCity;
    }

    /**
     * @return mixed
     */
    public function getBornCountry()
    {
        return $this->bornCountry;
    }

    /**
     * @param mixed $bornCountry
     */
    public function setBornCountry($bornCountry): void
    {
        $this->bornCountry = $bornCountry;
    }



    /**
     * @return string
     */
    public function getTown()
    {
        return $this->town;
    }

    /**
     * @param string $town
     */
    public function setTown( $town): void
    {
        $this->town = $town;
    }

    /**
     * @return string
     */
    public function getPhoneCode()
    {
        return $this->phoneCode;
    }

    /**
     * @param string $phoneCode
     */
    public function setPhoneCode( $phoneCode): void
    {
        $this->phoneCode = $phoneCode;
    }

    /**
     * @return string
     */
    public function getPhoneNumber()
    {
        return $this->phoneNumber;
    }

    /**
     * @param string $phoneNumber
     */
    public function setPhoneNumber( $phoneNumber): void
    {
        $this->phoneNumber = $phoneNumber;
    }

    /**
     * @return string
     */
    public function getPostalCode()
    {
        return $this->postalCode;
    }

    /**
     * @param string $postalCode
     */
    public function setPostalCode( $postalCode): void
    {
        $this->postalCode = $postalCode;
    }

    /**
     * @return mixed
     */
    public function getCountry()
    {
        return $this->country;
    }

    /**
     * @param mixed $country
     */
    public function setCountry($country): void
    {
        $this->country = $country;
    }

    /**
     * @return mixed
     */
    public function getState()
    {
        return $this->state;
    }

    /**
     * @param mixed $state
     */
    public function setState($state): void
    {
        $this->state = $state;
    }

    /**
     * @JMS\VirtualProperty
     * @JMS\Expose
     * @JMS\Groups("cards")
     * @JMS\SerializedName("bornCountry")
     */
    public function serializedBornCountry(){
        return $this->bornCountry? $this->bornCountry->getName(): '';
    }

    /**
     * @JMS\VirtualProperty
     * @JMS\Expose
     * @JMS\Groups("cards")
     * @JMS\SerializedName("country")
     */
    public function serializedCountry(){
        return $this->country? $this->country->getName(): '';
    }

    /**
     * @JMS\VirtualProperty
     * @JMS\Expose
     * @JMS\Groups("cards")
     * @JMS\SerializedName("state")
     */
    public function serializedState(){
        return $this->state? $this->state->getName(): '';
    }

    /**
     * @JMS\VirtualProperty
     * @JMS\Expose
     * @JMS\Groups("cards")
     *  @JMS\SerializedName("phone")
     */
    public function serializedPhone(){
     return   sprintf("%s %s", $this->phoneCode, $this->phoneNumber);
    }




}
