<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Entity;


use App\Enum\UserStatus;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\Security\Core\User\UserInterface;
use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as JMS;


/**
 *
 *
 * @ORM\Table(name="client")
 * @ORM\Entity
 * @JMS\ExclusionPolicy("ALL")
 */
class Client extends User
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     * @JMS\Expose
     *
     */
    protected $id;

    /**
     * @ORM\ManyToOne(targetEntity="Address")
     * @ORM\JoinColumn(name="address_id", referencedColumnName="id")
     * @JMS\Expose
     * @JMS\Groups("cards")
     * @JMS\Inline
     */
    protected $address;

    /**
     * @ORM\ManyToOne(targetEntity="Address")
     * @ORM\JoinColumn(name="shipping_address_id", referencedColumnName="id")
     *
     *
     */
    protected $shippingAddress;



    /**
     * @ORM\Column(name="document_number", type="string", length=100, nullable=true, unique=true)
     * @JMS\Expose
     * @JMS\Groups("cards")
     */
    protected $documentNumber;

    /**
     * @ORM\Column(name="card_type", type="string",  nullable=true)
     *
     */
    protected $cardType;

    /**
     * @ORM\Column(name="gender", type="string",  nullable=true)
     * @JMS\Expose
     * @JMS\Groups("cards")
     */
    protected $gender;

    /**
     * @ORM\Column(name="birth_date", type="date", nullable=true)
     * @JMS\Expose
     * @JMS\Groups("cards")
     * @JMS\Type("DateTime<'Y-m-d'>")
     */
    protected $birthDate;


    /**
     * @ORM\ManyToOne(targetEntity="Client")
     * @ORM\JoinColumn(name="parent_id", referencedColumnName="id")
     */

    protected $parent;





    /**
     * Constructor
     */
    public function __construct($params)
    {
        parent::__construct($params);
        $this->birthDate = isset($params['birthDate']) ?new \DateTime($params['birthDate'])  : $this->birthDate;
        $this->shippingAddress = isset($params['shippingAddress']) ? $params['shippingAddress'] : $this->shippingAddress;
        $this->address = isset($params['address']) ? $params['address'] : $this->address;
        $this->gender = isset($params['gender']) ? $params['gender'] : $this->gender;
        $this->cardType = isset($params['cardType']) ? $params['cardType'] : $this->cardType;
        $this->documentNumber = isset($params['passport']) ? $params['passport'] : $this->documentNumber;

    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getCardType()
    {
        return $this->cardType;
    }

    /**
     * @param mixed $cardType
     */
    public function setCardType($cardType): void
    {
        $this->cardType = $cardType;
    }


    /**
     * @return mixed
     */
    public function getAddress()
    {
        return $this->address;
    }

    /**
     * @param mixed $address
     */
    public function setAddress($address): void
    {
        $this->address = $address;
    }

    /**
     * @return mixed
     */
    public function getShippingAddress()
    {
        return $this->shippingAddress;
    }

    /**
     * @param mixed $shippingAddress
     */
    public function setShippingAddress($shippingAddress): void
    {
        $this->shippingAddress = $shippingAddress;
    }

    /**
     * @return mixed
     */
    public function getDocumentNumber()
    {
        return $this->documentNumber;
    }

    /**
     * @param mixed $documentNumber
     */
    public function setDocumentNumber($documentNumber): void
    {
        $this->documentNumber = $documentNumber;
    }

    /**
     * @return mixed
     */
    public function getDocumentType()
    {
        return $this->documentType;
    }

    /**
     * @param mixed $documentType
     */
    public function setDocumentType($documentType): void
    {
        $this->documentType = $documentType;
    }

    /**
     * @return mixed
     */
    public function getGender()
    {
        return $this->gender;
    }

    /**
     * @param mixed $gender
     */
    public function setGender($gender): void
    {
        $this->gender = $gender;
    }

    /**
     * @return mixed
     */
    public function getBirthDate()
    {
        return $this->birthDate;
    }

    /**
     * @param mixed $birthDate
     */
    public function setBirthDate($birthDate): void
    {
        $this->birthDate = $birthDate;
    }

    /**
     * @return mixed
     */
    public function getParent()
    {
        return $this->parent;
    }

    /**
     * @param mixed $parent
     */
    public function setParent($parent): void
    {
        $this->parent = $parent;
    }


    /**
     * @JMS\VirtualProperty
     * @JMS\Expose
     * @JMS\Groups("cards")
     * @JMS\SerializedName("cardType")
     */
    public function serializedCardType(){
        switch ($this->cardType){
            case 'plastic-usd':
                return 'USD';
            case 'plastic-eur':
                return 'EUR';
            default:
                return 'VIRTUAL';

        }
    }


    /**
     * @JMS\VirtualProperty
     * @JMS\Expose
     * @JMS\Groups("cards")
     * @JMS\SerializedName("birthDay")
     */
    public function serializedBirthDay(){
        return $this->birthDate ? $this->birthDate->format('d'): '';
    }

    /**
     * @JMS\VirtualProperty
     * @JMS\Expose
     * @JMS\Groups("cards")
     * @JMS\SerializedName("birthMonth")
     */
    public function serializedBirthMonth(){
        return $this->birthDate ? $this->birthDate->format('m'): '';
    }

    /**
     * @JMS\VirtualProperty
     * @JMS\Expose
     * @JMS\Groups("cards")
     * @JMS\SerializedName("birthYear")
     */
    public function serializedBirthYear(){
        return $this->birthDate ? $this->birthDate->format('Y'): '';
    }

    public function __toString()
    {
        return $this->getFirstName() . ' '. $this->getLastNameFather(). ' '. $this->getLastNameMother();
    }
}
