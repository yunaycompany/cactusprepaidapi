<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Entity;


use App\Enum\RolType;
use App\Enum\TipoDocumento;
use App\Enum\UserStatus;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as JMS;
use Symfony\Component\Security\Core\User\UserInterface;


/**
 *
 *
 * @ORM\Table(name="user")
 * @ORM\Entity(repositoryClass="App\Repository\UserRepository")
 * @JMS\ExclusionPolicy("ALL")
 * @ORM\InheritanceType("JOINED")
 * @ORM\DiscriminatorColumn(name="type", type="integer")
 * @ORM\DiscriminatorMap({"1"= "User","2" = "Client"})
 *
 */
class User implements UserInterface
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     * @JMS\Expose
     *
     *
     */
    protected $id;


    /**
     * @ORM\Column(name="first_name", type="string", length=100, nullable=true)
     * @JMS\Expose
     * @JMS\Groups("cards")
     * @JMS\SerializedName("firstName")
     */
    protected $firstName;


    /**
     * @ORM\Column(name="last_name", type="string", length=100, nullable=true)
     * @JMS\Expose
     * @JMS\Groups("cards")
     * @JMS\SerializedName("lastNameMother")
     */
    protected $lastNameMother;


    /**
     * @ORM\Column(name="last_name_father", type="string", length=100, nullable=true)
     * @JMS\Expose
     * @JMS\Groups("cards")
     * @JMS\SerializedName("lastNameFather")
     */
    protected $lastNameFather;

    /**
     * @var string
     *
     * @ORM\Column(name="email", type="string", length=150, nullable=false,unique=true)
     * @JMS\Expose
     * @JMS\Groups("cards")
     */
    protected $email;


    /**
     * @var string
     *
     * @ORM\Column(name="username", type="string", length=150, nullable=false, unique=true)
     * @JMS\Expose
     */
    protected $username;



    /**
     * @var string
     *
     * @ORM\Column(name="password", type="string", length=255, nullable=false)
     *
     */
    protected $password;


    /**
     * @var string
     *
     * @ORM\Column(name="salt", type="string", length=150, nullable=true)
     *
     */
    protected $salt;


    /**
     * @var string
     *
     * @ORM\Column(name="status", type="integer", nullable=true,  options={"default": 0})
     * @JMS\Expose
     * @JMS\Groups("cards")
     */
    protected $status;


    /**
     * @ORM\Column(name="ip", type="string", length=100, nullable=true)
     */
    protected $ip;


    /**
     * @ORM\Column(name="created", type="datetime", nullable=false)
     * @JMS\Expose
     * @JMS\Groups("cards")
     * @JMS\Type("DateTime<'Y-m-d H:i:s'>")
     */
    protected $created;


    /**
     * @ORM\Column(name="modified", type="datetime", nullable=true)
     */
    protected $modified;

    /**
     * @ORM\Column(name="password_requested_at", type="datetime", nullable=true)
     */
    protected $passwordRequestedAt;


    /**
     * @ORM\ManyToOne(targetEntity="User")
     * @ORM\JoinColumn(name="updated_user", referencedColumnName="id")
     */
    protected $updatedUser;



    /**
     * @ORM\Column(name="last_login", type="datetime", nullable=false)
     */
    protected $lastLogin;


    /**
     * @ORM\Column(name="confirmation_token",type="string",length=80, unique=true,nullable=true)
     */
    protected $confirmationToken;


    /**
     * @ORM\Column(name="roles",type="array", nullable=false)
     */
    protected $roles;


    /**
     * Constructor
     */
    public function __construct($params = [])
    {

        $this->created = new \DateTime();
        $this->lastLogin = new \DateTime();
        $this->status = UserStatus::INACTIVE;


        $this->roles = [];
        $this->setValues($params);
    }

    public function setValues($params)
    {
        $this->firstName = isset($params['firstName']) ? $params['firstName'] : $this->firstName;
        $this->firstName = isset($params['name']) ? $params['name'] : $this->firstName;
        $this->lastNameFather = isset($params['lastNameFather']) ? $params['lastNameFather'] : $this->lastNameFather;
        $this->lastNameMother = isset($params['lastNameMother']) ? $params['lastNameMother'] : $this->lastNameMother;
        $this->email = isset($params['email']) ? $params['email'] : $this->email;
        $this->status = isset($params['status']) ? $params['status'] : $this->status;
        $this->ip = isset($params['ip']) ? $params['ip'] : $this->ip;
        $this->updatedUser = isset($params['updatedUser']) ? $params['updatedUser'] : $this->updatedUser;

        $this->confirmationToken = isset($params['confirmationToken']) ? $params['confirmationToken'] : $this->confirmationToken;
        $emailParts = explode("@", $this->email);
        $this->username = isset($emailParts[0]) ? $emailParts[0] . '_' . time() : 'CLI_' . time();
        $this->username = isset($params['username']) && $params['username'] ? $params['username'] : $this->username;
        $this->modified = new \DateTime();
        $this->roles = isset($params['roles']) ? $params['roles'] : $this->roles;

        return $this;
    }


    public function eraseCredentials()
    {

    }


    public function addRole($role)
    {
        $role = strtoupper($role);
        if ($role === RolType::ROLE_DEFAULT) {
            return $this;
        }

        if (!in_array($role, $this->roles, true)) {
            $this->roles[] = $role;
        }

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function getRoles()
    {
        $roles = $this->roles;

        // we need to make sure to have at least one role
        $roles[] = RolType::ROLE_DEFAULT;

        return array_unique($roles);
    }


    public function hasRole($role)
    {
        return in_array(strtoupper($role), $this->getRoles(), true);
    }

    public function getSalt()
    {

        return null;
    }


    /**
     * @return mixed
     */
    public function getPasswordRequestedAt()
    {
        return $this->passwordRequestedAt;
    }

    /**
     * @param mixed $passwordRequestedAt
     */
    public function setPasswordRequestedAt($passwordRequestedAt)
    {
        $this->passwordRequestedAt = $passwordRequestedAt;
    }

    public function getUsername()
    {
        return $this->email;
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getFirstName()
    {
        return $this->firstName;
    }

    /**
     * @param mixed $firstName
     */
    public function setFirstName($firstName)
    {
        $this->firstName = $firstName;
    }

    /**
     * @return mixed
     */
    public function getLastNameMother()
    {
        return $this->lastNameMother;
    }

    /**
     * @param mixed $lastNameMother
     */
    public function setLastNameMother($lastNameMother): void
    {
        $this->lastNameMother = $lastNameMother;
    }

    /**
     * @return mixed
     */
    public function getLastNameFather()
    {
        return $this->lastNameFather;
    }

    /**
     * @param mixed $lastNameFather
     */
    public function setLastNameFather($lastNameFather): void
    {
        $this->lastNameFather = $lastNameFather;
    }


    /**
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @param string $email
     */
    public function setEmail($email)
    {
        $this->email = $email;
    }

    /**
     * @return string
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * @param string $password
     */
    public function setPassword($password)
    {
        $this->password = $password;
    }

    /**
     * @return string
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param string $status
     */
    public function setStatus($status)
    {
        $this->status = $status;
    }

    /**
     * @return mixed
     */
    public function getIp()
    {
        return $this->ip;
    }

    /**
     * @param mixed $ip
     */
    public function setIp($ip)
    {
        $this->ip = $ip;
    }

    /**
     * @return mixed
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * @param mixed $created
     */
    public function setCreated($created)
    {
        $this->created = $created;
    }

    /**
     * @return mixed
     */
    public function getModified()
    {
        return $this->modified;
    }

    /**
     * @param mixed $modified
     */
    public function setModified($modified)
    {
        $this->modified = $modified;
    }

    /**
     * @return mixed
     */
    public function getUpdatedUser()
    {
        return $this->updatedUser;
    }

    /**
     * @param mixed $updatedUser
     */
    public function setUpdatedUser($updatedUser)
    {
        $this->updatedUser = $updatedUser;
    }

    /**
     * @return mixed
     */
    public function getLastLogin()
    {
        return $this->lastLogin;
    }

    /**
     * @param mixed $lastLogin
     */
    public function setLastLogin($lastLogin)
    {
        $this->lastLogin = $lastLogin;
    }

    /**
     * @return mixed
     */
    public function getConfirmationToken()
    {
        return $this->confirmationToken;
    }

    /**
     * @param mixed $confirmationToken
     */
    public function setConfirmationToken($confirmationToken)
    {
        $this->confirmationToken = $confirmationToken;
    }


    public function getDisplayName()
    {
        return $this->firstName . ' ' . $this->lastNameFather. ' '. $this->lastNameMother;
    }

    public function getFirstRole()
    {
        return $this->getRoles()[0];
    }

}
