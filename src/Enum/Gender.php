<?php

/**
 * Created by PhpStorm.
 * User: Yosbel Fonseca Mendoza
 * Date: 31/03/2017
 * Time: 23:35
 */

namespace App\Enum;


class Gender
{

    const F = 1;
    const M = 2;
}
