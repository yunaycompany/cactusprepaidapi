<?php

/**
 * Created by PhpStorm.
 * User: Yosbel Fonseca Mendoza
 * Date: 31/03/2017
 * Time: 23:35
 */

namespace App\Enum;

/**
 * Mapeo de los codigos http posibles
 * Class HttpCode
 */
class HttpCode
{

    //2XX SUCCESSFUL OPERATIONS
    const HTTP_OK = 200;
    const HTTP_CREATED = 201;
    const HTTP_UNOFFICIALI = 202;
    const HTTP_UNOFFICIALIII = 203;
    const HTTP_DELETED = 204;
    const HTTP_RELOAD = 205;
    const HTTP_PARCIAL = 206;
    //3XX REDIRECTIONS
    const HTTP_MOVED_PERMANENTLY = 301;
    const HTTP_MOVED_FOUNDED = 302;
    const HTTP_MOVED_OTHERS = 303;
    const HTTP_UNMODIFIED = 304;
    const HTTP_USE_PROXY = 305;
    const HTTP_MOVED_PARTIALLY = 307;
    //4XX CLIENT ERRORS
    const HTTP_WRONG_REQUEST = 400;
    const HTTP_UNAUTHORIZED = 401;
    const HTTP_PAYMENT_REQUIRED = 402;
    const HTTP_FORBIDDEN = 403;
    const HTTP_RESOURCE_NOTFOUND = 404;
    const HTTP_CONFLICT = 409;
    const HTTP_NOT_AVAILABLE = 410;
    const HTTP_PRECONDITION_FAILED = 412;
    //5XX  SERVER ERRORS
    const HTTP_SERVER_ERROR = 500;
    const HTTP_NOT_IMPLEMENTED = 501;
    const HTTP_BAD_GATEWAY = 502;
    const HTTP_SERVER_UNAVAILABLE = 503;
    const HTTP_GATEWAY_TIMEOUT = 504;
    const HTTP_UNSUPPORTED_VERSION = 505;
    //Codigos showii empiezan por 100
    const INVALID_DATA = 100; // Cuando esta mal email o algun otro dato en la creacion de usuarios
    const DUPLICATE_ENTRY_FOR_KEY = 101; // Cuando ya existe el email al registrar usuario
    const DUPLICATE_ENTRY_PASSPORT = 108; // Cuando ya existe el email al registrar usuario

    const NOT_FOUND_EMAIL = 102; //No existe email recuuperar password
    const NOT_SENDED_EMAIL = 103; //No se envio el email
    const NOT_CONFIRMED = 104; //Personal no confirmado
    const MARCA_REQUIRED = 105; //La marca es requerida
    const SIN_STOCK = 106; //La marca es requerida
    const PEDIDO_SIN_DETALLE = 107; //La marca es requerida
}
