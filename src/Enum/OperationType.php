<?php

/**
 * Created by PhpStorm.
 * User: Yosbel Fonseca Mendoza
 * Date: 31/03/2017
 * Time: 23:35
 */

namespace App\Enum;


class OperationType
{

    const ADD_FUNDS = 1;
    const SEND_MONEY = 2;
    const WITHDRAW = 3;
    const CRYPTO = 4;
    const BUY_DOLAR = 5;
    const BUY_SELL = 6;
    const EXCHANGE = 7;

}
