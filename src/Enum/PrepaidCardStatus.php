<?php

/**
 * Created by PhpStorm.
 * User: Yosbel Fonseca Mendoza
 * Date: 31/03/2017
 * Time: 23:35
 */

namespace App\Enum;


class PrepaidCardStatus
{

    const INACTIVE = 0;
    const ACTIVE = 1;
    const BLOCKED = 2;
    const DELETED = 3;

}
