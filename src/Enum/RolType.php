<?php

/**
 * Created by PhpStorm.
 * User: Yosbel Fonseca Mendoza
 * Date: 31/03/2017
 * Time: 23:35
 */

namespace App\Enum;


class RolType
{

    const ROLE_DEFAULT = 'ROLE_CLIENT';
    const ROLE_ADMIN = 'ROLE_ADMIN';
    const ROLE_SUPER_ADMIN = 'ROLE_SUPER_ADMIN';

}
