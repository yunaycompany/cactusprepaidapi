<?php

/**
 * Created by PhpStorm.
 * User: Yosbel Fonseca Mendoza
 * Date: 31/03/2017
 * Time: 23:35
 */

namespace App\Enum;


class TransactionStatus
{

    const PENDING = 1;
    const SUCCESS = 2;
    const CANCELLED = 3;

}
