<?php

/**
 * Created by PhpStorm.
 * User: Yosbel Fonseca Mendoza
 * Date: 31/03/2017
 * Time: 23:35
 */

namespace App\Enum;


class TypeCurrency
{

    const BTC = 1;
    const USD = 2;
    const ARS = 3;
    const BRL = 4;
    const EUR = 5;

}
