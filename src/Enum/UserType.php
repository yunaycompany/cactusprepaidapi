<?php

/**
 * Created by PhpStorm.
 * User: Yosbel Fonseca Mendoza
 * Date: 31/03/2017
 * Time: 23:35
 */

namespace App\Enum;


class UserType
{

    const ADMIN = 1;
    const CLIENT = 2;

}
