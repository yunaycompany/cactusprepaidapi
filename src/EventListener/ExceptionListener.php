<?php

namespace App\EventListener;

/**
 * Description of Listener
 *
 * @author yosbel
 */

use App\Exception\BadRequestException;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Event\ExceptionEvent;
use Symfony\Component\HttpKernel\Event\GetResponseForExceptionEvent;
use \Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\HttpKernel\Exception\HttpExceptionInterface;
use Symfony\Component\Security\Core\Exception\AuthenticationException;
use App\Enum\HttpCode;
use App\Entity\Trace;
use Doctrine\ORM\EntityManagerInterface;

class ExceptionListener extends \Exception
{

    private $em;

    public function setEntityManager(EntityManagerInterface $em)
    {
        $this->em = $em;
    }

    public function onKernelException(ExceptionEvent $event)
    {

        $error = [];
        $error['status']=false;
        $isApi= strpos($event->getRequest()->getRequestUri(),'v1/');
        if($isApi){
            $exception = $event->getThrowable();

            $message = sprintf(
                'Error: %s with code: %s',
                $exception->getMessage(),
                $exception->getCode()
            );
            // Customize your response object to display the exception details
            $error["message"] = $message;
            $response = new JsonResponse();
            $response->setContent($message);
            // HttpExceptionInterface is a special type of exception that
            // holds status code and header details
            if ($exception instanceof BadRequestException) {
                $error["code"] = $exception->getStatusCode();
                $response->setStatusCode(Response::HTTP_BAD_REQUEST);
                $response->headers->replace($exception->getHeaders());
            }else  if ($exception instanceof HttpExceptionInterface) {
                $error["code"] = $exception->getStatusCode();
                $response->setStatusCode($exception->getStatusCode());
                $response->headers->replace($exception->getHeaders());
            } else if($exception->getCode()) {
                $error["code"] = $exception->getCode();
                $response->setStatusCode($exception->getCode());
            } else {
                $error["code"] = Response::HTTP_INTERNAL_SERVER_ERROR;
                $response->setStatusCode(Response::HTTP_INTERNAL_SERVER_ERROR);
            }
//        $this->traceException($event->getRequest(), $error);
            $response->setData($error);
            $event->setResponse($response);

        }

    }

    private function traceException($request, $error)
    {
        if ($this->em->isOpen()) {
            $trace = new Trace();
            $trace->setIp($request->getClientIp());
            $trace->setAction("Exception");
            $trace->setEmail("Anonimo");
            $url = $request->getMethod() . $request->getPathInfo();
            $trace->setRoute($url);
            if ($request->headers->get('GT')) {
                $error["header"] = $request->headers->get('GT');
            } else
                if ($request->headers->get('FT')) {
                    $error["header"] = $request->headers->get('FT');
                } else
                    if ($request->headers->get('Authorization')) {
                        $error["header"] = $request->headers->get('Authorization');
                    }
            $params = $request->request->all();
            $data = array_merge($params, $error);
            $trace->setData(json_encode($data));
            $this->em->persist($trace);
            $this->em->flush();
        }
    }

}
