<?php

namespace App\EventListener;

use Lexik\Bundle\JWTAuthenticationBundle\Event\JWTNotFoundEvent;
use Lexik\Bundle\JWTAuthenticationBundle\Event\JWTInvalidEvent;
use Lexik\Bundle\JWTAuthenticationBundle\Event\JWTExpiredEvent;
use Lexik\Bundle\JWTAuthenticationBundle\Event\JWTCreatedEvent;
use Lexik\Bundle\JWTAuthenticationBundle\Event\JWTDecodedEvent;
use Lexik\Bundle\JWTAuthenticationBundle\Response\JWTAuthenticationFailureResponse;

use Symfony\Component\HttpFoundation\RequestStack;

class JWTListener
{

	private $request;

    public function __construct(RequestStack $request){
        $this->request = $request;
    }

	public function onJWTNotFound(JWTNotFoundEvent $event)
	{
		$response = new JWTAuthenticationFailureResponse('Missing session', 401);
		$event->setResponse($response);
	}

	public function onJWTInvalid(JWTInvalidEvent $event)
	{
		$response = new JWTAuthenticationFailureResponse('Your session is invalid, please login again.', 401);
		$event->setResponse($response);
	}

	public function onJWTExpired(JWTExpiredEvent $event)
	{
		$response = new JWTAuthenticationFailureResponse('Your session is expired, please login again.', 401);
		$event->setResponse($response);
	}

	public function onJWTCreated(JWTCreatedEvent $event)
	{
        $payload = $event->getData();
		$payload['ip'] = $this->request->getCurrentRequest()->getClientIp();
        $event->setData($payload);
	}

	public function onJWTDecoded(JWTDecodedEvent $event)
	{
		$payload = $event->getPayload();
		if (!isset($payload['ip']) || $payload['ip'] !== $this->request->getCurrentRequest()->getClientIp()) {
			$event->markAsInvalid();
		}
	}
}
