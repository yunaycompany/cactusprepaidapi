<?php

namespace App\EventListener;

use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Doctrine\ORM\Event\OnFlushEventArgs;
use App\Entity\Trace;
use Symfony\Component\Finder\Finder;
use Symfony\Component\Security\Core\User\UserInterface;

class RegisterListener implements ContainerAwareInterface {

    protected $container;
    protected $email;
    protected $ip;
    protected $route;
    protected $moreData = [];

    const ACTION_CREATE = 'Create';
    const ACTION_UPDATE = 'Update';
    const ACTION_DELETE = 'Delete';

    public function setContainer(ContainerInterface $container = null) {
        $this->container = $container;
    }

    private function getData($event) {
        $this->email = "Anonimo";
        $this->ip = "";
        $this->route = "";

        if ($this->container->get('request_stack')->getCurrentRequest()) {
            $request = $this->container->get('request_stack')->getCurrentRequest();
            $this->ip = $request->getClientIp();
            $this->route = $request->getMethod() . $request->getPathInfo();
            $this->moreData["params"] = $request->request->all();
            if ($request->headers->get('GT')) {
                $this->moreData["header"] = "GT " . $request->headers->get('GT');
            } else
            if ($request->headers->get('FT')) {
                $this->moreData["header"] = "FT " . $request->headers->get('FT');
            } else if ($request->headers->get('Authorization')) {
                $this->moreData["header"] = "Authorization " . $request->headers->get('Authorization');
            }
        }

        $securityContext = $this->container->get('security.token_storage', ContainerInterface::NULL_ON_INVALID_REFERENCE);

        if (null !== $securityContext && null !== $securityContext->getToken()) {
            $token = $securityContext->getToken();
            if ($token && $token->getUser() && $token->getUser() instanceof  UserInterface) {
                $this->email = $token->getUser()->getEmail();
            }
        }
    }

    /**
     * Obteniendo el nombre de la Entidad
     */
    public function getEntityName($entity) {
        $className = join('', array_slice(explode("\\", get_class($entity)), -1));
        return $className;
    }

    public function onFlush(OnFlushEventArgs $event) {
        $this->getData($event);

        $em = $event->getEntityManager();
        $uow = $em->getUnitOfWork();

        foreach ($uow->getScheduledEntityInsertions() AS $entity) {
            if (!$entity instanceof Trace) {
                $data = $uow->getEntityChangeSet($entity);
                $this->insertData($entity, self::ACTION_CREATE, $em, $data);
            }
        }

        foreach ($uow->getScheduledEntityUpdates() AS $entity) {
            $data = $uow->getEntityChangeSet($entity);
            $this->updateData($entity, self::ACTION_UPDATE, $em, $data);
        }

        foreach ($uow->getScheduledEntityDeletions() AS $entity) {
            $data = $uow->getEntityChangeSet($entity);
            $this->updateData($entity, self::ACTION_DELETE, $em, $data);
        }
    }

    public function insertData($class, $action, $em, $data = null) {
        $className = $this->getEntityName($class);
        if ($this->containClass($className)) {
            $data['clase'] = $className;
            $uow = $em->getUnitOfWork();
            $trace = new Trace();
            $trace->setIp($this->ip);
            $trace->setAction($action);
            $trace->setEmail($this->email);
            $trace->setRoute($this->route);
            $allData = array_merge($data, $this->moreData);
            $trace->setData(json_encode($allData));
            $em->persist($trace);
            $traceMeta = $em->getClassMetadata(get_class($trace));
            $uow->computeChangeSet($traceMeta, $trace);
        }
    }

    public function updateData($class, $action, $em, $data = null) {
        $className = $this->getEntityName($class);
        if ($this->containClass($className)) {
            $id = $class->getId();
            $data['id'] = $id;
            $data['clase'] = $className;
            $uow = $em->getUnitOfWork();
            $trace = new Trace();
            $trace->setIp($this->ip);
            $trace->setAction($action);
            $trace->setEmail($this->email);
            $trace->setRoute($this->rute);
            $allData = array_merge($data, $this->moreData);
            $trace->setData(json_encode($allData));
            $em->persist($trace);
            $traceMeta = $em->getClassMetadata(get_class($trace));
            $uow->computeChangeSet($traceMeta, $trace);
        }
    }

    /**
     * Verifica si la entidad que se modifica se encuentra marcada
     */
    public function containClass($entity) {
        $finder = new Finder();
        $dirEntity = __DIR__ . '/../Entity/';
        $entityExt = $entity . ".php";
        $finder->in($dirEntity)->files()->name($entityExt)->contains("@Traza");
        return $finder->count() > 0 ? true : false;
    }

}
