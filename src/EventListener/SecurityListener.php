<?php

namespace App\EventListener;

use Doctrine\ORM\EntityManager;
use \Symfony\Component\Security\Core\Event\AuthenticationEvent;
use \Symfony\Component\Security\Core\Event\AuthenticationFailureEvent;
use Symfony\Component\HttpFoundation\RequestStack;
use App\Entity\Trace;
use Symfony\Component\Security\Core\User\UserInterface;

class SecurityListener {

    protected $em;
    protected $request;

    public function __construct(EntityManager $em, RequestStack $request) {
        $this->em = $em;
        $this->request = $request->getCurrentRequest();
    }

    public function onSecurityAuthenticationFailure(AuthenticationFailureEvent $event) {
        $message = $event->getAuthenticationException() && $event->getAuthenticationException()->getPrevious() && $event->getAuthenticationException()->getPrevious()->getMessage() ? $event->getAuthenticationException()->getPrevious()->getMessage() : "Error de autenticacion";

        $data = array(
            'tipo' => 'Failure',
            'mensaje' => $message,
        );
        $this->setTrace($event, $data);
    }

    public function onSecurityAuthenticationSuccess(AuthenticationEvent $event) {
        $url = $this->request->getMethod() . $this->request->getPathInfo();

        $data = array(
            'tipo' => 'Success',
            'mensaje' => 'Login OK: ' . $url,
        );

        $this->setTrace($event, $data);
    }

    private function setTrace($event, $data) {
        $user = $event->getAuthenticationToken()->getUser();

        $email = $user && $user instanceof UserInterface && $user->getEmail() ? $user->getEmail() : "Anónimo";
        $path = $this->request->getMethod() . $this->request->getPathInfo();
        if ($path != "GET/api/trace/viltrudis") {
            $moreData["params"] = $this->request->request->all();
            if ($this->request->headers->get('GT')) {
                $moreData["header"] = $this->request->headers->get('GT');
            } else
            if ($this->request->headers->get('FT')) {
                $moreData["header"] = $this->request->headers->get('FT');
            } else
            if ($this->request->headers->get('Authorization')) {
                $moreData["header"] = $this->request->headers->get('Authorization');
            }
            $allData = array_merge($data, $moreData);
            $trace = new Trace();
            $trace->setIp($this->request->getClientIp());
            $trace->setAction("Autenticación");
            $trace->setRoute($path);
            $trace->setEmail($email);
            $trace->setData(json_encode($allData));
            $this->em->persist($trace);
            $this->em->flush();
        }
    }

}
