<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20191007134731 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE user (id INT AUTO_INCREMENT NOT NULL, updated_user INT DEFAULT NULL, first_name VARCHAR(100) DEFAULT NULL, last_name VARCHAR(100) DEFAULT NULL, email VARCHAR(150) NOT NULL, username VARCHAR(150) NOT NULL, password VARCHAR(255) NOT NULL, salt VARCHAR(150) DEFAULT NULL, status INT DEFAULT 0, ip VARCHAR(100) DEFAULT NULL, created DATETIME NOT NULL, modified DATETIME DEFAULT NULL, password_requested_at DATETIME DEFAULT NULL, last_login DATETIME NOT NULL, confirmation_token VARCHAR(80) DEFAULT NULL, roles LONGTEXT NOT NULL COMMENT \'(DC2Type:array)\', type INT NOT NULL, UNIQUE INDEX UNIQ_8D93D649E7927C74 (email), UNIQUE INDEX UNIQ_8D93D649F85E0677 (username), UNIQUE INDEX UNIQ_8D93D649C05FB297 (confirmation_token), INDEX IDX_8D93D649DE6FB3BE (updated_user), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE client (id INT NOT NULL, nationality_id INT DEFAULT NULL, state_id INT DEFAULT NULL, parent_id INT DEFAULT NULL, document_number VARCHAR(100) DEFAULT NULL, document_type INT DEFAULT NULL, gender INT DEFAULT NULL, birth_date DATE DEFAULT NULL, address VARCHAR(250) DEFAULT NULL, zip VARCHAR(100) DEFAULT NULL, UNIQUE INDEX UNIQ_C744045528F2AE32 (document_number), INDEX IDX_C74404551C9DA55 (nationality_id), INDEX IDX_C74404555D83CC1 (state_id), INDEX IDX_C7440455727ACA70 (parent_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE operation_type (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(100) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE currency (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(100) NOT NULL, code VARCHAR(50) NOT NULL, enabled TINYINT(1) DEFAULT NULL, modified DATETIME DEFAULT NULL, created DATETIME NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE state (id INT AUTO_INCREMENT NOT NULL, country_id INT DEFAULT NULL, name VARCHAR(100) NOT NULL, code VARCHAR(50) DEFAULT NULL, enabled INT DEFAULT 1 NOT NULL, created DATETIME NOT NULL, updated DATETIME NOT NULL, INDEX IDX_A393D2FBF92F3E70 (country_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE card_brand (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(100) NOT NULL, created DATETIME NOT NULL, updated DATETIME NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE trace (id INT AUTO_INCREMENT NOT NULL, ip VARCHAR(50) NOT NULL, created_date DATETIME DEFAULT NULL, email VARCHAR(255) DEFAULT NULL, route VARCHAR(255) DEFAULT NULL, description VARCHAR(255) DEFAULT NULL, status INT DEFAULT NULL, action VARCHAR(255) DEFAULT NULL, data LONGTEXT DEFAULT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE prepaid_card (id INT AUTO_INCREMENT NOT NULL, brand_id INT DEFAULT NULL, name VARCHAR(100) NOT NULL, card_number VARCHAR(255) NOT NULL, cardholder_name VARCHAR(255) NOT NULL, status INT DEFAULT 1, created DATETIME NOT NULL, updated DATETIME NOT NULL, INDEX IDX_22A55BC244F5D008 (brand_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE country (id INT AUTO_INCREMENT NOT NULL, currency_id INT DEFAULT NULL, name VARCHAR(100) NOT NULL, region VARCHAR(100) NOT NULL, phone VARCHAR(15) NOT NULL, code VARCHAR(50) NOT NULL, enabled INT DEFAULT 1 NOT NULL, created DATETIME NOT NULL, updated DATETIME NOT NULL, INDEX IDX_5373C96638248176 (currency_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE user ADD CONSTRAINT FK_8D93D649DE6FB3BE FOREIGN KEY (updated_user) REFERENCES user (id)');
        $this->addSql('ALTER TABLE client ADD CONSTRAINT FK_C74404551C9DA55 FOREIGN KEY (nationality_id) REFERENCES country (id)');
        $this->addSql('ALTER TABLE client ADD CONSTRAINT FK_C74404555D83CC1 FOREIGN KEY (state_id) REFERENCES state (id)');
        $this->addSql('ALTER TABLE client ADD CONSTRAINT FK_C7440455727ACA70 FOREIGN KEY (parent_id) REFERENCES client (id)');
        $this->addSql('ALTER TABLE client ADD CONSTRAINT FK_C7440455BF396750 FOREIGN KEY (id) REFERENCES user (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE state ADD CONSTRAINT FK_A393D2FBF92F3E70 FOREIGN KEY (country_id) REFERENCES country (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE prepaid_card ADD CONSTRAINT FK_22A55BC244F5D008 FOREIGN KEY (brand_id) REFERENCES card_brand (id)');
        $this->addSql('ALTER TABLE country ADD CONSTRAINT FK_5373C96638248176 FOREIGN KEY (currency_id) REFERENCES currency (id) ON DELETE CASCADE');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE user DROP FOREIGN KEY FK_8D93D649DE6FB3BE');
        $this->addSql('ALTER TABLE client DROP FOREIGN KEY FK_C7440455BF396750');
        $this->addSql('ALTER TABLE client DROP FOREIGN KEY FK_C7440455727ACA70');
        $this->addSql('ALTER TABLE country DROP FOREIGN KEY FK_5373C96638248176');
        $this->addSql('ALTER TABLE client DROP FOREIGN KEY FK_C74404555D83CC1');
        $this->addSql('ALTER TABLE prepaid_card DROP FOREIGN KEY FK_22A55BC244F5D008');
        $this->addSql('ALTER TABLE client DROP FOREIGN KEY FK_C74404551C9DA55');
        $this->addSql('ALTER TABLE state DROP FOREIGN KEY FK_A393D2FBF92F3E70');
        $this->addSql('DROP TABLE user');
        $this->addSql('DROP TABLE client');
        $this->addSql('DROP TABLE operation_type');
        $this->addSql('DROP TABLE currency');
        $this->addSql('DROP TABLE state');
        $this->addSql('DROP TABLE card_brand');
        $this->addSql('DROP TABLE trace');
        $this->addSql('DROP TABLE prepaid_card');
        $this->addSql('DROP TABLE country');
    }
}
