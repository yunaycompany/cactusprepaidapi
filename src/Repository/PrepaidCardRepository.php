<?php

namespace App\Repository;

use App\Domain\TypeAward;
use App\Domain\TypeAwardStatus;
use App\Domain\TypeDevice;
use App\Domain\TypeNotifications;
use App\Domain\TypeProfile;
use App\Domain\TypeProfileState;
use App\Domain\TypeUserStatus;
use App\Enum\PrepaidCardStatus;
use Doctrine\ORM\EntityRepository;

/**
 * Description of PrepaidCardRepository
 *
 * @author yosbel
 */
class PrepaidCardRepository extends EntityRepository
{


    public function findUserCards($user, $isAdmin = false)
    {
        $em = $this->getEntityManager();
        $qb = $em->createQueryBuilder();

        $qb = $qb->select('pc.cardNumber, pc.cardHolderName,  pc.created, b.name as brand, pc.status')
            ->from('App:PrepaidCard', 'pc')
            ->innerJoin('pc.client', 'c')
            ->innerJoin('pc.brand', 'b');
        if (!$isAdmin) {
            $qb->andWhere("pc.status = :status")
                ->andWhere('pc.client = :clientId OR c.parent = :clientId')
                ->setParameter('clientId', $user->getId())
                ->setParameter('status', PrepaidCardStatus::ACTIVE);
        }


        return $qb->getQuery()
            ->getArrayResult();

    }


}
