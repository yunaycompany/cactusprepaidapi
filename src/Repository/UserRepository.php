<?php

namespace App\Repository;

use App\Domain\TypeAward;
use App\Domain\TypeAwardStatus;
use App\Domain\TypeDevice;
use App\Domain\TypeNotifications;
use App\Domain\TypeProfile;
use App\Domain\TypeProfileState;
use App\Domain\TypeUserStatus;
use App\Entity\Client;
use Doctrine\ORM\EntityRepository;

/**
 * Description of UserRepository
 *
 * @author yosbel
 */
class UserRepository extends EntityRepository
{

    public function findUserByConfirmationToken($token)
    {
        $em = $this->getEntityManager();
        $qb = $em->createQueryBuilder();

        $user = $qb->select('u')
            ->from('App:User', 'u')
            ->where("u.confirmationToken = :token")
            ->setParameter('token', $token)
            ->setMaxResults(1)
            ->getQuery()
            ->getOneOrNullResult();
        return $user;
    }

    public function findUserCards($user){
        $em = $this->getEntityManager();
        $qb = $em->createQueryBuilder();

        $user = $qb->select('u')
            ->from('App:User', 'u')
            ->where("u.confirmationToken = :token")
            ->setParameter('token', '')
            ->setMaxResults(1)
            ->getQuery()
            ->getOneOrNullResult();
        return $user;
    }

    public function findCardsRequest(){
        $em = $this->getEntityManager();
        $qb = $em->createQueryBuilder();

        $user = $qb->select('u')
            ->from('App:Client', 'u')
            ->getQuery()
            ->getResult();
        return $user;
    }


}
