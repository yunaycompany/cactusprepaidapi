<?php

namespace App\Services;

use Symfony\Component\Routing\Generator\UrlGeneratorInterface;

use Aws\Ses\Exception\SesException;
use App\Domain\TypeProductState;
use Twig\Environment;

class RestMailer
{

    protected $router;
    protected $twig;
    protected $env;
    protected $siteName;
    protected $mailer;

    public function __construct(\Swift_Mailer $mailer, UrlGeneratorInterface $router, Environment $twig, $env, $siteName)
    {

        $this->router = $router;
        $this->twig = $twig;
        $this->env = $env;
        $this->siteName = $siteName;
        $this->mailer = $mailer;
    }


    public function sendEmail($subject, $user, $template, $data = null, $admin = false)
    {

        $message = (new \Swift_Message($subject))
            ->setSubject($subject)
            ->setFrom(array('info@cactusprepaid.com' => $this->siteName))
            ->setTo($user->getEmail())
            ->setBody(
                $this->twig->render(
                    'emails/' . $template . '.html.twig',
                    array(
                        'user' => $user,
                        'data' => $data
                    )
                ), 'text/html'
            );
         $this->mailer->send($message);

         if($admin){
             $template='card_request_admin';
             $message = (new \Swift_Message($subject))
                 ->setSubject($subject)
                 ->setFrom(array('info@cactusprepaid.com' => $this->siteName))
                 ->setTo('info@cactusprepaid.com')
                 ->setBody(
                     $this->twig->render(
                         'emails/' . $template . '.html.twig',
                         array(
                             'user' => $user,
                             'data' => $data
                         )
                     ), 'text/html'
                 );
             return $this->mailer->send($message);
         }

    }
    public function sendAdminEmail($subject, $data = '')
    {
        $data = is_array($data) ? json_encode($data) : $data;
        $template = "<p> Data del email: </p></br>" . $data;
        $message = (new \Swift_Message($subject))
            ->setSubject($subject)
            ->setFrom(array('info@cactusprepaid.com' => $this->siteName))
            ->setTo('info@cactusprepaid.com')
            ->setBody($template, 'text/html'
            );
        return $this->mailer->send($message);
    }

}
