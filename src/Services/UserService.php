<?php

namespace App\Services;

use App\Entity\Address;
use App\Entity\Client;
use App\Entity\Country;
use App\Entity\State;
use App\Enum\UserStatus;
use JMS\Serializer\SerializationContext;
use JMS\Serializer\SerializerInterface;
use Symfony\Component\HttpFoundation\RequestStack;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpKernel\Exception\UnauthorizedHttpException;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use \App\Enum\HttpCode;
use App\Entity\User;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Lexik\Bundle\JWTAuthenticationBundle\Services\JWTTokenManagerInterface;
use Lexik\Bundle\JWTAuthenticationBundle\Response\JWTAuthenticationFailureResponse;
use Symfony\Component\Security\Core\User\UserInterface;

class UserService
{

    private $em;
    private $passwordEncoder;
    private $utilService;
    private $request;
    private $restMailer;
    private $user;
    private $serializer;

    private $jwtManager;

    public function __construct(EntityManagerInterface $entityManager,
                                UtilService $utilService,
                                RequestStack $request,
                                RestMailer $restMailer,
                                SerializerInterface $serializer,
                                UserPasswordEncoderInterface $passwordEncoder,
                                TokenStorageInterface $tokenStorage,
                                JWTTokenManagerInterface $jwtManager

    )
    {

        $this->em = $entityManager;
        $this->jwtManager = $jwtManager;
        $this->passwordEncoder = $passwordEncoder;
        $this->utilService = $utilService;
        $this->request = $request;
        $this->serializer=$serializer;
        $this->restMailer = $restMailer;

        $this->user = $tokenStorage && $tokenStorage->getToken()  ? $tokenStorage->getToken()->getUser() : null;
    }

    public function loginUser($params)
    {


        $email = isset($params['email']) && $params['email'] ? $params['email'] : "";
        $password = isset($params['password']) && $params['password'] ? $params['password'] : "";


        $regUser = $this->em->getRepository(User::class)->findOneBy(['email' => $email]);

        if (!$regUser) {
            throw new UnauthorizedHttpException("invalid", "invalid");
        }


        if (!$this->passwordEncoder->isPasswordValid($regUser, $password)) {
            throw new UnauthorizedHttpException("invalid.", "invalid.");
        }

        if ($regUser->getStatus() == UserStatus::BLOCKED) {
            throw new  UnauthorizedHttpException("blocked", "blocked");
        }
        if ($regUser->getStatus() != UserStatus::ACTIVE) {
            throw new UnauthorizedHttpException("disabled", 'disabled');
        }



        $regUser->setIp($this->request->getCurrentRequest()->getClientIp());
        $token = $this->jwtManager->create($regUser);


        $this->em->persist($regUser);
        $this->em->flush();
        $response['token'] = $token;
        $response['displayName'] = $regUser->getDisplayName();
        $response['photoURL'] ='';
        $response['userRole'] = $regUser->getFirstRole();
        return $response;


    }

    public function createCardRequest($params){
        $email = isset($params['person']['email']) && $params['person']['email'] ? $params['person']['email'] : "";
        $passport = isset($params['person']['passport']) && $params['person']['passport'] ? $params['person']['passport'] : "";
        $user = $this->em->getRepository(User::class)->findOneBy(['email' => $email]);
        $client = $this->em->getRepository(Client::class)->findOneBy(['documentNumber' => $passport]);
        if ($user) {
            throw new \Exception("Username Exist", HttpCode::DUPLICATE_ENTRY_FOR_KEY);
        }

        if($client){
            throw new \Exception("Username Exist", HttpCode::DUPLICATE_ENTRY_PASSPORT);
        }

        $userParams['confirmationToken'] = sha1($this->utilService->generateToken());
        $userParams['token'] = sha1($this->utilService->generateToken());
        $userParams['ip'] = $this->request->getCurrentRequest()->getClientIp();
        $userParams=array_merge($userParams, $params['person']);
        $userParams['cardType']= isset($params['cardType'])?$params['cardType']: '';

        $client= new Client($userParams);



        $addressParams=isset($params['address'])?$params['address']: null;
        $countryId = $addressParams&& isset($addressParams['country']) ?$addressParams['country']: null ;
        $bornCountryId = $addressParams&& isset($addressParams['bornCountry']) ?$addressParams['bornCountry']: null ;
        $stateId = $addressParams&& isset($addressParams['state']) ?$addressParams['state']: null ;
        $addressParams['country'] = $countryId? $this->em->getRepository(Country::class)->find($countryId): null;
        $addressParams['bornCountry'] = $bornCountryId? $this->em->getRepository(Country::class)->find($bornCountryId): null;
        $addressParams['state'] = $stateId? $this->em->getRepository(State::class)->find($stateId): null;

        $shippingAddressParams=isset($params['shippingAddress'])?$params['shippingAddress']: null;
        $countryId = $shippingAddressParams&& isset($shippingAddressParams['country']) ?$shippingAddressParams['country']: null ;
        $stateId = $shippingAddressParams&& isset($shippingAddressParams['state']) ?$shippingAddressParams['state']: null ;
        $shippingAddressParams['country'] = $countryId? $this->em->getRepository(Country::class)->find($countryId): null;
        $shippingAddressParams['state'] = $stateId? $this->em->getRepository(State::class)->find($stateId): null;



        if($shippingAddressParams){
            $shippingAddress= new Address($shippingAddressParams);
            $this->em->persist($shippingAddress);
            $client->setShippingAddress($shippingAddress);
        }

        if($addressParams){
            $address= new Address($addressParams);
            $this->em->persist($address);
            $client->setAddress($address);
        }

        $client->setPassword($this->passwordEncoder->encodePassword($client, uniqid()));
        $this->em->persist($client);
        $this->em->flush();
        try{
            $res = $this->restMailer->sendEmail('Card Request in Process!', $client, 'card_request', $client, true);
        }catch (\Exception $ex){
        }

        $response['status'] = 'OK';
        return $response;
    }

    public function registerUser($params)
    {
        $email = isset($params['email']) && $params['email'] ? $params['email'] : "";
        $user = $this->em->getRepository(User::class)->findOneBy(['email' => $email]);
        $params['hash'] = $this->utilService->generateToken();
        if ($user) {
            throw new \Exception("Username Exist", HttpCode::DUPLICATE_ENTRY_FOR_KEY);
        }

        $params['confirmationToken'] = sha1($this->utilService->generateToken());
        $params['token'] = sha1($this->utilService->generateToken());
        $params['ip'] = $this->request->getCurrentRequest()->getClientIp();


        $newUser = new Client($params);
        $newUser->addRole('ROLE_CLIENT');
        $newUser->setPassword($this->passwordEncoder->encodePassword($newUser, $params['password']));
        $this->em->persist($newUser);
        $this->em->flush();

        $res = $this->restMailer->sendEmail('Bienvenido a Cactus Prepaid!', $newUser, 'signup', null, false);
        $response['status'] = 'OK';
        return $response;
    }

    public function contact($params)
    {

        $this->restMailer->sendAdminEmail('Mensaje desde el Formulario de Contacto!', $params);
        $response['status'] = 'OK';
        return $response;
    }

    public function getCardsRequest(){
        $clients=  $this->em->getRepository(User::class)->findCardsRequest();

        $context= SerializationContext::create();
        $context->setGroups("cards");
      return  $this->serializer->toArray($clients,$context);

    }


    public function changeUserData($params)
    {
        $userId = isset($params['id']) ? $params['id'] : '';


        $user = $this->em->getRepository(User::class)->find($userId);
        if ($user) {
            $user->setValues($params);
            $password = isset($params['password']) && $params['password'] ? $this->passwordEncoder->encodePassword($user, $params['password']) : null;
            if ($password) {
                $user->setPassword($password);
            }
            $this->em->persist($user);
            $this->em->flush();
        }
        $response['status'] = 'OK';
        return $response;
    }

    public function findUserBy($property)
    {
        return $this->em->getRepository(User::class)->find($property);
    }

    public function confirm($token)
    {

        if (!$token) {
            $response["code"] = HttpCode::HTTP_WRONG_REQUEST;
            return $response;
        }

        $user = $this->em->getRepository(User::class)->findUserByConfirmationToken($token);
        if (!$user) {
            $response["code"] = HttpCode::HTTP_WRONG_REQUEST;
            return $response;
        }


        if ($user->getStatus() == UserStatus::ACTIVE) {
            $response["code"] = 'already_confirmed';

        } else {
            $user->setStatus(UserStatus::ACTIVE);
            $this->em->persist($user);
            $this->em->flush();
            $response["code"] = 'confirmed';
        }
      return $response;
    }


    public function recoveryPassword($params)
    {
        $email = isset($params['email']) ? $params['email'] : "";
        if ($regUser = $this->em->getRepository(User::class)->findOneBy(['email' => $email])) {
            if ($regUser->getConfirmationToken() == NULL) {
                $regUser->setConfirmationToken(sha1($this->utilService->generateToken()));
                $regUser->setPasswordRequestedAt(new \DateTime());
                $this->em->persist($regUser);
                $this->em->flush();
            }
            if ($regUser instanceof User && $regUser->getStatus() == UserStatus::ACTIVE) {
                $this->restMailer->sendEmail('Recuperar contraseña', $regUser, 'forgot');
                return array('status' => 'OK');
            } else if ($regUser instanceof User) {
                $this->restMailer->sendEmail('Confirmar Email', $regUser, 'signup');
                return array('status' => 'error', 'response' => 'Please confirm the email we sent before resetting your password.');
            } else {
                return array('status' => 'error', 'response' => $regUser);
            }
        } else {
            throw new HttpException(409, "recoveryPassword - invalid");
        }
    }

    public function resetPassword($params)
    {
        $confirmationToken = isset($params['confirmation_token']) ? $params['confirmation_token'] : '';
        $password = isset($params['password']) ? $params['password'] : '';
        if ($regUser = $this->em->getRepository(User::class)->findUserByConfirmationToken($confirmationToken)) {
            $regUser->setPassword($this->passwordEncoder->encodePassword($regUser, $password));
            $regUser->setConfirmationToken(null);
            $regUser->setPasswordRequestedAt(null);
            $this->em->persist($regUser);
            $this->em->flush();
            return $regUser;
        } else {
            throw new HttpException(409, "resetPassword - invalid");
        }
    }


    public function checkUser(User $user, $password)
    {
        return $this->passwordEncoder->isPasswordValid($user, $password);
    }




    public function changePassword($params)
    {
        $oldPassword = isset($params['confirmation_token']) ? $params['confirmation_token'] : "";
        $password = isset($params['password']) ? $params['password'] : "";
        if ($regUser = $this->em->getRepository(User::class)->findOneBy(['email' => $this->user->getEmail()])) {
            if ($this->passwordEncoder->isPasswordValid($regUser, $oldPassword)) {
                $regUser->setPassword($this->passwordEncoder->encodePassword($regUser, $password));
                $this->em->persist($regUser);
                $this->em->flush();
                return true;
            } else {
                throw new HttpException(409, 'Old Password don\'t match');
            }
        } else {
            throw new HttpException(409, "changePassword - User not exist");
        }
    }


}
