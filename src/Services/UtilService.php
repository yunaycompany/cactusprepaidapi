<?php


namespace App\Services;
use Hashids\Hashids;

class UtilService
{

    private $dictionary;
    private $hashId;

    /**
     * UtilService constructor.
     */
    public function __construct($hashId)
    {
        $this->hashId = $hashId;
        $this->dictionary = 'ABCDEFGHIJKLMNPQRSTUVWXYZ123456789';
    }

    private function crypto_rand_secure($min, $max)
    {
        $range = $max - $min;
        if ($range == 0)
            return $min; // not so random...
        $log = log($range, 2);
        $bytes = (int)($log / 8) + 1; // length in bytes
        $bits = (int)$log + 1; // length in bits
        $filter = (int)(1 << $bits) - 1; // set all lower bits to 1
        do {
            $rnd = hexdec(bin2hex(openssl_random_pseudo_bytes($bytes, $s)));
            $rnd = $rnd & $filter; // discard irrelevant bits
        } while ($rnd >= $range);
        return $min + $rnd;
    }

    private function getToken($length)
    {
        $token = "";
        $codeAlphabet = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
        $codeAlphabet .= "abcdefghijklmnopqrstuvwxyz";
        $codeAlphabet .= "0123456789";
        $max = strlen($codeAlphabet); // edited

        for ($i = 0; $i < $length; $i++) {
            $token .= $codeAlphabet[$this->crypto_rand_secure(0, $max - 1)];
        }

        return $token;
    }

    public function getSecureToken($length = 16)
    {
        if (function_exists('openssl_random_pseudo_bytes')) {
            $bytes = openssl_random_pseudo_bytes($length * 2);

            if ($bytes === false) {
                // throw exception that unable to create random token
            }

            return substr(str_replace(array('/', '+', '='), '', base64_encode($bytes)), 0, $length);
        }

        return $this->getToken($length);
    }

    /**
     * {@inheritdoc}
     */
    public function generateToken()
    {
        return rtrim(strtr(base64_encode(random_bytes(32)), '+/', '-_'), '=');
    }


    public function curl($url, $headers = array('Content-type: application/json'), $method = 'GET', $data = array())
    {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_TIMEOUT, 30);
        if ($method == 'POST') {
            curl_setopt($ch, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($data));
        }
        if ($method == 'jsonPOST') {
            curl_setopt($ch, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($data));
        }
        $result = curl_exec($ch);

        if ($result === false) {
            $error = curl_error($ch);
//            $this->logger->error('Curl Error', $data);
            return array('error' => 'Not available');
        }
        curl_close($ch);
        if ($json = json_decode($result, true)) {
            return $json;
        }
        return $result;
    }


    /**
     * Types 1 for users ids, 2 transactions ids
     * @param type $str
     * @param type $type
     * @return type
     */
    public function encodeHash($str, $type = 0)
    {
        $hashids = new Hashids($this->hashId, 8, $this->dictionary);
        if ($type) {
            return $hashids->encode(array($type, $str));
        } else {
            return $hashids->encode($str);
        }

    }

    public function decodeHash($str)
    {
        $hashids = new Hashids($this->hashId, 8, $this->dictionary);
        return $hashids->decode($str);
    }


}
